const inputs = [
    {
        label: "Voornaam",
        type: "text",
        required: true,
        minLength: 3,
        maxLength: 32,
        disabled: false,
        labelText: "Voornaam",
        regex: /^[a-zA-Z_\-]+$/
    },
    {
        label: "Middennaam",
        type: "text",
        required: false,
        minLength: 2,
        maxLength: 0,
        disabled: false,
        labelText: "Middennaam",
        regex: /^[a-zA-Z]{3,40}(?:\s[A-Z])?\s[a-zA-Z]{3,40}$/
    },
    {
        label: "Achternaam",
        type: "text",
        required: true,
        minLength: 3,
        maxLength: 32,
        disabled: false,
        labelText: "Achternaam",
        regex: /^[a-zA-Z_\-]+$/
    },
    {
        label: "Postcode",
        type: "text",
        required: true,
        minLength: 6,
        maxLength: 6,
        disabled: false,
        labelText: "Postcode",
        regex: /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i
    },
    {
        label: "Woonplaats",
        type: "text",
        required: true,
        minLength: 0,
        maxLength: 32,
        disabled: false,
        labelText: "Woonplaats",
        regex: /^[a-zA-Z_\-]+$/
    },
    {
        label: "Adres",
        type: "text",
        required: true,
        minLength: 3,
        maxLength: 64,
        disabled: false,
        labelText: "Adres",
        regex: /(?:\d+[a-z]*)$/
    },
    {
        label: "Email",
        type: "email",
        required: true,
        minLength: 3,
        maxLength: 32,
        disabled: false,
        labelText: "Email",
        regex: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    },
    {
        label: "Telefoon",
        type: "tel",
        required: true,
        minLength: 8,
        maxLength: 16,
        disabled: false,
        labelText: "Telefoon",
        regex: /^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/i
    },
    {
        label: "Radio",
        type: "radio",
        required: true,
        labelText: "Geslacht",
        options: {
            "man": [
                {
                    label: "car",
                    type: "select",
                    required: false,
                    minLength: 8,
                    maxLength: 16,
                    disabled: false,
                    labelText: "Favorite Auto Merk",
                    options: [
                        "Audi",
                        "Volswagen",
                        "BMW",
                        "SAAB",
                        "Opel",
                        "Masarati",
                        "Andere"
                    ]
                },
            ],
            "vrouw" : [
                {
                    label: "band",
                    type: "select",
                    required: false,
                    minLength: 8,
                    maxLength: 16,
                    disabled: false,
                    labelText: "Favorite Boy band",
                    options: [
                        "One direction",
                        "andere"
                    ]
                },
            ],
            "onzijdig" : [
                {
                    label: "car",
                    type: "select",
                    required: false,
                    minLength: 8,
                    maxLength: 16,
                    disabled: false,
                    labelText: "Favorite Auto Merk",
                    options: [
                        "Audi",
                        "Volswagen",
                        "BMW",
                        "SAAB",
                        "Opel",
                        "Masarati",
                        "Andere"
                    ]
                },
                {
                    label: "band",
                    type: "select",
                    required: false,
                    minLength: 8,
                    maxLength: 16,
                    disabled: false,
                    labelText: "Favorite Boy band",
                    options: [
                        "One direction",
                        "andere"
                    ]
                },
            ]
        }
    },
    {
        label: "GeboorteDatum",
        type: "date",
        required: true,
        minLength: 8,
        maxLength: 16,
        disabled: false,
        labelText: "Geboorte Datum",
    },
    {
        label: "Submit",
        type: "submit",
        required: false,
        minLength: 8,
        maxLength: 16,
        disabled: false,
        labelText: "",
        regex: ""
    }
];