/*
 |  Build the form
 */

inputs.forEach(field => {

    let div = $("<div class='form-group'>")
        .attr("id", "d" + field.label);

    if (field.type === "radio") {

        const label = $("<label>")
            .attr("for", field.label)
            .attr("id", "l" + field.label)
            .append(field.labelText + (field.required ? '*' : null));

        div.append(label);

        Object.keys(field.options).map((value) => {
            let radioHolder = $("<div class='checkbox'>");

            const label = $("<label>")
                .attr("for", value)
                .append(value);

            let elem = $("<input>")
                .attr("type", "radio")
                .attr("name", field.label)
                .attr("value", value)
                .attr("id", value);

            elem.on('click', (event) => {
                $("#qdiv").empty();
                field.options[value].forEach((field) => {
                    addInputElement(field, "#qdiv")
                    console.log(field);
                });
                console.log(event.target.value);
            });

            radioHolder.append(elem);
            radioHolder.append(label);
            div.append(radioHolder)
        });

        $("#form").append(div);
        let questionDiv = $("<div >")
            .attr("id", "qdiv");
        $("#form").append(questionDiv);
        return;
    }

    addInputElement(field, "#form");
});

function addInputElement(field, target) {
    target = $(target);

    let div = $("<div class='form-group'>")
        .attr("id", "d" + field.label);

    const label = $("<label>")
        .attr("for", field.label)
        .attr("id", "l" + field.label)
        .append(field.labelText + (field.required ? '*' : ''));

    target.append(label);

    if (field.type === "select") {
        let elem = $("<select class=\"form-control\">")
            .attr("name", field.label);

        field.options.forEach((option) => {
            let tag = $("<option>")
                .attr("value", option)
                .append(option);
            elem.append(tag);
        })

        target.append(elem);

        $("#form").append(div);
        return;
    }

    let elem = $("<input class=\"form-control\">")
        .attr("id", field.label)
        .attr("name", field.label)
        .attr("type", field.type);
    target.append(elem);

    $("#form").append(div);
}

/*
 |  Add events to elements
 */

$(document).on('input', 'input', (event) => {
    checkItem(event.target.id);
});

$(document).on('blur', 'input', (event) => {
    checkItem(event.target.id);
});

$(document).on('submit', '#form', () => {
    return false;
});

$(document).on('submit', '#form', () => {
    checkForm();
});

/*
 |  Functions
 */
function checkItem(elementId) {
    inputs.forEach(field => {
        if (elementId === field.label) {
            if (!field.required) {
                return;
            }

            checkRegex(field);
        }
    });
}

function checkForm() {
    inputs.forEach(field => {
        if (!field.required) {
            return;
        }

        checkRegex(field);
    });
}

function checkRegex(field) {

    const label = $("#d" + field.label);
    const inputData = $("#" + field.label);
    label.has("span").find("span").remove();

    if (field.required && inputData[0].value == "") {
        const a = $("<span>")
            .addClass("text-danger")
            .text(field.label + " is vereist");
        label.append(a);
        return;
    }

    if (field.regex !== "") {
        let regex = RegExp(field.regex);
        if (!regex.test(inputData[0].value)) {
            console.log(regex);
            const a = $("<span>")
                .addClass("text-danger")
                .text(field.label + " voldoet niet aan de eisen");
            label.append(a);
        }
    }
}